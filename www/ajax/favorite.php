<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
header('Content-type:application/json; charset=UTF-8');
/*
 *  Работа с избранным по ajax
 *  Всегда возвращает json со значением ret (int)
 *  Принимает 2 параметра @id и @action
 *  @id - идетификатор квартиры
 *  @action - строка с указанием действия [ add, del ] Добавление или удаление квартиры из избанного
 *
 *  Возвращает количество квартир в избранном
 */

$data['ret'] = 0;

use Bitrix\Estate as Estate;

if (!CModule::IncludeModule("iblock")) {
    $this->AbortResultCache();
    ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
    return;
}

$requiredModules = array('estate');
foreach ($requiredModules as $requiredModule) {
    if (!CModule::IncludeModule($requiredModule)) {
        ShowError(GetMessage('F_NO_MODULE'));
        return;
    }
}

$flatId = (int)$_REQUEST['id'] ? (int)$_REQUEST['id'] : 0;
$arActions = array('add', 'del');
$action = '';
if (in_array($_REQUEST['action'], $arActions)) {
    $action = $_REQUEST['action'];
}

$favorite = Estate\EstateFavoriteTable::getInstance();

if ($flatId && $action) {
    if ($action === 'add') {
        $favorite->addFavoriteFlat($flatId);
    }
    if ($action === 'del') {
        $favorite->removeFavoriteFlat($flatId);
    }
}

$data['ret'] = $favorite->getFavoriteFlatsCount();


echo json_encode($data);
