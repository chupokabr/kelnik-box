<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
header('Content-type:application/json; charset=UTF-8');

$data = [];

if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
    echo json_encode($data);
    return;
}

if (!CModule::IncludeModule('iblock')) {
    echo json_encode($data);
    return;
}

$APPLICATION->IncludeComponent("kelnik:gallery.estate", "ajax", array(
        "IBLOCK_ID" => 12,
        "BUILDING_IBLOCK_ID" => 10,
        "OBJECT_ID" => $_REQUEST['object_id'],
        "PERIOD" => $_REQUEST['period'],
        "gallery" => $_REQUEST['gallery']
    )
    ,
    false
);

exit;