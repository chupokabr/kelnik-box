<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
header('Content-type:application/json; charset=UTF-8');

$json = array('html' => '');

$price = isset($_REQUEST['price'])
         ? (int) str_replace(' ', '', $_REQUEST['price'])
         : false;
$firstPayment = isset($_REQUEST['payment'])
                ? (int) str_replace(' ', '', $_REQUEST['payment'])
                : false;
$time = isset($_REQUEST['time']) ? (int) $_REQUEST['time'] : false;

if (!$price || !$time || !$firstPayment) {
    echo json_encode($json);
    return;
}

if (!CModule::IncludeModule("iblock")) {
    echo json_encode($json);
    return;
}

//ob_start();
$APPLICATION->IncludeComponent("kelnik:mortgage", ".default",
    array(
        'TYPE'      => 'mortgage'
    )
);
$json['html'] = ob_get_clean();


$res = CIBlockElement::GetList(array(), array(
	'IBLOCK_ID'=>8,
	'ACTIVE_DATE'=>'Y',
), false, false, array(
	'ID',
	'IBLOCK',
	'ACTIVE_TO',
	'PROPERTY_MIN',
	'PROPERTY_RATE',
));
if($ob = $res->Fetch()){


	$date1 = new DateTime($ob['ACTIVE_TO']);
	$date2 = new DateTime("now");
	$interval = $date1->diff($date2);
	$diffMonth = $interval->format('%m');
	$diffMonth += ($interval->format('%y'))*12;

	$timeText = $diffMonth. ' '.plural($diffMonth, array('месяц','месяца','месяцев'));

	$minPaymentPersent = $ob['PROPERTY_MIN_VALUE'];
	$rate = $ob['PROPERTY_RATE_VALUE'];



	$instPayment = ($firstPayment >= $price * ($minPaymentPersent/100))
		? $firstPayment
		: round($price * ($minPaymentPersent/100));

	$remain = $price - $instPayment;

	$pow = pow(1 + $rate / 1200, -($diffMonth/12) * 12 + 1);
	$payment = round(($remain * $rate / 1200) / (1 - $pow));

	$timeText = $diffMonth. ' '.plural($diffMonth, array('месяц','месяца','месяцев'));
	$json['installments'] = array(
		'payment'      => price_format($payment) . ' рублей',
		'firstPayment' => price_format($instPayment) . '  рублей',
		'time'         => $timeText,
	);
}






echo json_encode($json);
