<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
header('Content-type:application/json; charset=UTF-8');

$data['ret'] = 0;

if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
    echo json_encode($data);
    return;
}

if(!CModule::IncludeModule('iblock')) {
    echo json_encode($data);
    return;
}

if (empty($_REQUEST['name']) || empty($_REQUEST['phone'])) {
    echo json_encode($data);
    return;
}

$time = 'В течение 15 минут';
if (!empty($_REQUEST['choose-time'])) {
    $days = array('Сегодня', 'Завтра', 'Послезавтра');
    $time = !empty($_REQUEST['choose-day']) && in_array($_REQUEST['choose-day'], $days)
            ? $_REQUEST['choose-day']
            : $days[0];

    $lowdate = !empty($_REQUEST['lowdate']) ? (int) $_REQUEST['lowdate'] : 9;
    $update = !empty($_REQUEST['update']) ? (int) $_REQUEST['update'] : 9;
    $time .= $lowdate === $update ? ' в ' : ' с ';
    $time .= $lowdate . ':00';
    if ($lowdate !== $update) {
        $time .= ' до ' . $update . ':00';
    }
}

$el = new CIBlockElement;
$message = <<<MESS
ФИО: {$_REQUEST['name']}
Контактный телефон: {$_REQUEST['phone']}
Удобное время звонка: $time
MESS;
$arLoadProductArray = Array(
    "IBLOCK_ID"         => 1,
    "MODIFIED_BY"       => $USER->GetID(),
    "NAME"              => $_REQUEST['name'],
    "DETAIL_TEXT"       => $message,
    "DATE_ACTIVE_FROM"  => date('d.m.Y H:i:s'),
);
$el->Add($arLoadProductArray);

$arEventFields = array(
    'NAME'  => $_REQUEST['name'],
    'PHONE' => $_REQUEST['phone'],
    'TIME'  => $time,
);
CEvent::Send('CALL_REQUEST', 's1', $arEventFields, 'N');
$data['ret'] = 1;

echo json_encode($data);
