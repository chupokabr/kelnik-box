<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
header('Content-type:application/json; charset=UTF-8');

$json = array('result' => 0);

if (!CModule::IncludeModule("iblock")) {
    echo json_encode($json);
    return;
}

$requiredModules = array('estate');
foreach ($requiredModules as $requiredModule) {
    if (!CModule::IncludeModule($requiredModule)) {
        echo json_encode($json);
        return;
    }
}

use Bitrix\Estate as Estate;

// Карта активных элементов недвижимости
$activeMap = Estate\BaseEstate::getActiveElementsMap();

$flatsInstance = Estate\EstateFlatTable::getInstance();

if (!empty($_REQUEST['getResult'])) {
    parse_str($_REQUEST['getResult'], $params);
    $_REQUEST += $params;
}

$filter = $flatsInstance->getSearchRequest();
if (empty($filter['TYPE'])) {
    $filter['TYPE'] = 999;
}

//if ($_REQUEST['division'] === 'homepage' && isset($filter['TYPE'])) {
//    unset($filter['TYPE']);
//}

if ($_REQUEST['division'] !== 'floor') {
    // Количество квартир на этажах
    $floors = $flatsInstance->getCntByFloors($filter);
    foreach ($floors as $ID => &$floor) {
        $floor['PARENT'] = $activeMap['FLOORS'][$ID]['PARENT'];
    }
    unset($floor);
}

switch ($_REQUEST['division']) {
    case 'homepage':
        $objId = Estate\BaseEstate::MAIN_OBJECT_ID;
        if ((int)$_REQUEST['objectId']) {
            $objId = (int)$_REQUEST['objectId'];
        }
        // Данные объекта
        $res = Estate\EstateObjectTable::getById($objId);
        $object = $res->fetch();

        // $sectionIds = Estate\BaseEstate::getParentsFromResult($floors);
        // $sections = array_intersect_key($activeMap['SECTIONS'], array_flip($sectionIds));
        $buildingIds = Estate\BaseEstate::getParentsFromResult($floors);
        /*$buildings = Estate\EstateBuildingTable::getAssoc(
            array('filter' => array('ID' => $buildingIds)),
            'ID'
        );*/

        $buildingInstance = Estate\EstateBuildingTable::getInstance();
        $json = $buildingInstance->getJson($object, $buildingIds, $floors);
        break;

    case 'building':
        if (empty($_REQUEST['num'])) {
            $json['status'] = false;
            break;
        }
        // Данные корпуса
        $res = Estate\EstateBuildingTable::getById($_REQUEST['num']);
        $building = $res->fetch();
        if (!$building) {
            $json['status'] = false;
            break;
        }

        //Этажи связанных корпусов
        if ($building['LINK_SECTIONS']) {
            $linkSections = unserialize($building['LINK_SECTIONS']);
            $linkFilter = $filter;
            $linkFloors = Estate\BaseEstate::filterByParentId(
                $activeMap['FLOORS'],
                $linkSections
            );
            $linkFilter['PARENT'] = array_keys($linkFloors);
            $linkFloors = $flatsInstance->getCntByFloors($linkFilter);
            $floors += $linkFloors;
        }

        $floorInstance = Estate\EstateFloorTable::getInstance();
        $json = $floorInstance->getJson($building, $floors);

        break;

    // case 'section':
    //     if (empty($_REQUEST['num'])) {
    //         $json['status'] = false;
    //         break;
    //     }
    //     // Данные секции
    //     $res = Estate\EstateSectionTable::getById($_REQUEST['num']);
    //     $section = $res->fetch();
    //     if (!$section) {
    //         $json['status'] = false;
    //         break;
    //     }

    //     // Этажи связанных секций
    //     if ($section['LINK_SECTIONS']) {
    //         $linkSections = unserialize($section['LINK_SECTIONS']);
    //         $linkFilter = $filter;
    //         $linkFloors = Estate\BaseEstate::filterByParentId(
    //             $activeMap['FLOORS'],
    //             $linkSections
    //         );
    //         $linkFilter['PARENT'] = array_keys($linkFloors);
    //         $linkFloors = $flatsInstance->getCntByFloors($linkFilter);
    //         $floors += $linkFloors;
    //     }

    //     $floorInstance = Estate\EstateFloorTable::getInstance();
    //     $json = $floorInstance->getJson($section, $floors);
    //     break;

    case 'floor':
        if (empty($_REQUEST['num'])) {
            $json['status'] = false;
            break;
        }
        // Данные этажа
        $res = Estate\EstateFloorTable::getById($_REQUEST['num']);
        $floor = $res->fetch();
        if (!$floor) {
            $json['status'] = false;
            break;
        }

        $json = $flatsInstance->getJson($floor, $filter);
        break;

    default:
        $json['status'] = false;
        break;
}

echo json_encode($json);
