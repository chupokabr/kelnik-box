<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
header('Content-type:application/json; charset=UTF-8');

$json = array('result' => 0);

if (!CModule::IncludeModule("iblock")) {
    echo json_encode($json);
    return;
}

$requiredModules = array('estate');
foreach ($requiredModules as $requiredModule) {
    if (!CModule::IncludeModule($requiredModule)) {
        echo json_encode($json);
        return;
    }
}


if (!isset($_REQUEST['FLAT_ID']) || !(int)$_REQUEST['FLAT_ID']) {
    @define("ERROR_404", "Y");
    CHTTP::SetStatus("404 Not Found");
    return;
}


use Bitrix\Estate as Estate;


// Карта активных элементов недвижимости
$activeMap = Estate\BaseEstate::getActiveElementsMap();

// Данные квартиры
$flatsInstance = Estate\EstateFlatTable::getInstance();
$arResult['FLAT'] = $flatsInstance->getFullFlatInfo($_REQUEST['FLAT_ID']);

// Активен ли родительский этаж
if (!isset($activeMap['FLOORS'][$arResult['FLAT']['PARENT']])) {
    @define("ERROR_404", "Y");
    CHTTP::SetStatus("404 Not Found");
    return;
}

// Карта активных элементов недвижимости
$activeMap = Estate\BaseEstate::getActiveElementsMap();

// Данные квартиры
$flatsInstance = Estate\EstateFlatTable::getInstance();
$arResult['FLAT'] = $flatsInstance->getFullFlatInfo($_REQUEST['FLAT_ID']);

// Активен ли родительский этаж
if (!isset($activeMap['FLOORS'][$arResult['FLAT']['PARENT']])) {
    @define("ERROR_404", "Y");
    CHTTP::SetStatus("404 Not Found");
    return;
}


// Параметры отображения списка квартир
$countOnPage = Estate\EstateFlatTable::COUNT_ON_PAGE;
$page = !empty($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
$offset = $countOnPage * $page - $countOnPage;

// Ссылка для возврата
$catalogPath = 'http://' . $_SERVER['HTTP_HOST'] . Estate\BaseEstate::ESTATE_HOME_PATH;
$searchPage = $catalogPath;
$visualPage = $catalogPath . 'visual/';
$visualPage = $flatsInstance->getVisualFloorLink($visualPage, $arResult['FLAT']);


// Получаем список этажей очереди
$stage = $arResult['FLAT']['BUILDING']['STAGE'];
$stageBuildingIds = array();
foreach ($activeMap['BUILDINGS'] as $ID => $building) {
    if ($building['STAGE'] === $stage) {
        $stageBuildingIds[] = $ID;
    }
}
$stageSections = Estate\BaseEstate::filterByParentId(
    $activeMap['SECTIONS'],
    $stageBuildingIds
);
$stageFloors = Estate\BaseEstate::filterByParentId(
    $activeMap['FLOORS'],
    array_keys($stageSections)
);

$arResult['HOME_PATH'] = Estate\BaseEstate::ESTATE_SEARCH_PATH;

$params = array(
//    'order' => array('ID' => 'ASC'),
    'limit' => $countOnPage,
    'offset' => $offset,
    'filter' => array(
        'PARENT' => array_keys($stageFloors),
        'TYPE' => $arResult['FLAT']['TYPE'],
        'STATUS' => Estate\EstateFlatTable::IN_SALE_STATUS,
        '>AREA_TOTAL' => $arResult['FLAT']['AREA_TOTAL'] + 0.001,
    ),
);

// Более просторные квартиры
$arResult['ITEMS'] = $flatsInstance->getSameFlats($params, $arResult['FLAT']);

unset($params['offset']);
$cnt = $flatsInstance->getSameFlats($params, $arResult['FLAT']);
$cnt = $cnt['CNT'];

$countLeft = $cnt - $countOnPage;
if ($countLeft > $countOnPage) {
    $countLeft = $countOnPage;
}
$arResult['NEXT_BTN_CNT'] = $countLeft;
$arResult['NEXT_BTN_CNT_WORD'] = plural($arResult['NEXT_BTN_CNT'], array('предложение', 'предложения', 'предложений'));


$json['nextBtnText'] = 'Посмотреть еще ' . $arResult['NEXT_BTN_CNT'] . ' ' . $arResult['NEXT_BTN_CNT_WORD'] . ' →';


ob_start();
include $_SERVER['DOCUMENT_ROOT'] . '/local/components/kelnik/estate.recommend/templates/.default/inc_flats.php';
$json['html'] = ob_get_clean();

$request = $_GET;
$request['page'] = $page + 1;
$json['nextUrl'] = $cnt > $offset + $countOnPage
    ? $_SERVER['PHP_SELF'] . '?' . http_build_query($request)
    : '';

echo json_encode($json);
