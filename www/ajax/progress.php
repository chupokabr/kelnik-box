<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
header('Content-type:application/json; charset=UTF-8');
CModule::IncludeModule("iblock");

setlocale(LC_ALL, "ru_RU.UTF-8");

$json = array();
$res = CIBlockElement::GetList(
	Array(

	),
	array(
		"IBLOCK_ID" => 9,
		"ID" => $_REQUEST['id']
	),
	false,
	false,
	Array(
		"ID",
		"IBLOCK_ID",
		"NAME",
		"PROPERTY_FILE",
		"ACTIVE_FROM",
	)
);
if ($ob = $res->fetch()) {

//	$from = MakeTimeStamp($ob['ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS");
//	$json['date'] = strftime("%Y, %B", $from);
	$json['date'] = $ob['NAME'];

	foreach($ob['PROPERTY_FILE_VALUE'] as $k => $item){
		$thumb = \CFile::ResizeImageGet(
			$item,
			array('width' => 190, 'height' => 120),
			BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
			true
		);
		$img = \CFile::ResizeImageGet(
			$item,
			array('width' => 1920, 'height' => 1080),
			BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
			true
		);
		$json['images'][] = array(
			"img" => $img['src'],
			"thumb" => $thumb['src'],
			"caption" => $ob['PROPERTY_FILE_DESCRIPTION'][$k]
		);
	}

}

//echo json_encode($json,JSON_FORCE_OBJECT);
echo json_encode($json);


