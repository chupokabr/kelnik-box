<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
header('Content-type:application/json; charset=UTF-8');

//$json = array('result' => 0);

if (!CModule::IncludeModule("iblock")) {
    echo json_encode($json);
    return;
}

$requiredModules = array('estate');
foreach ($requiredModules as $requiredModule) {
    if (!CModule::IncludeModule($requiredModule)) {
        echo json_encode($json);
        return;
    }
}


use Bitrix\Estate as Estate;


// Карта активных элементов недвижимости
$activeMap = Estate\BaseEstate::getActiveElementsMap();
$searchInstance = Estate\EstateSearch::getInstance();

// Поисковые фильтры
$filter = $searchInstance->getSearchRequest();
//if (empty($_REQUEST['building'])) {
//    $filter['PARENT'] = array_keys($activeMap['FLOORS']);
//}
//Формируем результаты по каждому ЖК
$activeMapObjects = Estate\BaseEstate::getIblockObjectsFullInfoWithBuilding();

$floorIds = $activeMap['FLOORS'];
if ($_REQUEST['objects']) {
    $reqObjects = $_REQUEST['objects'];
    if (is_array($reqObjects)) {
        $filterObjects = array();
        $reqObjects = array_keys($reqObjects);
        foreach ($reqObjects as $iObjId) {
            $filterObjects[] = $activeMapObjects[$iObjId]['OBJECT_ID'];
        }
        unset($iObjId);
        $buildingIds = Estate\BaseEstate::filterByParentId($activeMap['BUILDINGS'], $filterObjects);
//            $sectionIds = Estate\BaseEstate::filterByParentId($activeMap['SECTIONS'], array_keys($buildingIds));
        $floorIds = Estate\BaseEstate::filterByParentId($activeMap['FLOORS'], array_keys($buildingIds));
        if (count($floorIds)) {
            $filter['PARENT'] = array_keys($floorIds);
        }
    }
}

$cnt = $searchInstance->getResultCount($filter);

if (isset($_REQUEST['getInfo'])) {
    $json['result'] = (int)$cnt;
    $cntWord = plural($cnt, array('квартиру', 'квартиры', 'квартир'));
    $json['count'] = $cnt
        ? 'Показать ' . $cnt . ' ' . $cntWord
        : 'Нет подходящих квартир';

    // Умный фильтр
    $borders = $searchInstance->getFilterBorders($filter, true);
    $json['visible'] = array();
    foreach ($borders as $name => $values) {
        foreach ($values as $key => $val) {
            if (in_array($name, array('PRICE', 'AREA'))) {
                if ($name === "PRICE") {
                    if (strtolower($key) == 'min') {
                        $val = floor($val / 100000) * 100000;
                    }
                    if (strtolower($key) == 'max') {
                        $val = ceil($val / 100000) * 100000;
                    }
                }
                if (strtolower($key) == 'min') {
                    $val = price_format(floor($val));
                }
                if (strtolower($key) == 'max') {
                    $val = price_format(ceil($val));
                }
                $json[strtolower($name)][strtolower($key)] = $val;
            } else {
                $json['visible'][] = strtolower($name) . '[' . $val . ']';
            }
        }

    }
} else {

    $arResult['ACTIVE_SORT'] = !empty($_REQUEST['sort']) ? htmlspecialcharsex($_REQUEST['sort']) : 'price';
    $arResult['ACTIVE_ORDER'] = !empty($_REQUEST['order']) ? (int)$_REQUEST['order'] : 'asc';

    // Формируем результаты по каждому ЖК
    $activeMapObjects = Estate\BaseEstate::getIblockObjectsFullInfoWithBuilding();

    // Если задан объект то устанавливаем его
    if (!empty((int)$_REQUEST['object']) && isset($activeMapObjects[$_REQUEST['object']])) {
        $obj = array();
        $obj[$_REQUEST['object']] = $activeMapObjects[$_REQUEST['object']];
        $activeMapObjects = $obj;
    }

    $favorite = Estate\EstateFavoriteTable::getInstance();
    $favoriteIds = $favorite->getFavoriteFlats();

    $json['building'] = array();
    foreach ($activeMapObjects as $object) {
        $arResult['RESULT'][$object['ID']]['INFO'] = $object;

        //Устанавливаем фильтр по данному объекту
        Estate\BaseEstate::setObject($object['ID']);
        // Карта активных элементов недвижимости
        $activeMap = Estate\BaseEstate::getActiveElementsMap();

        // Поисковые фильтры
        $filter['PARENT'] = array_keys($activeMap['FLOORS']);


        // Параметры отображения списка квартир
        $countOnPage = Estate\EstateFlatTable::COUNT_ON_PAGE;
        $page = !empty($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $offset = $countOnPage * $page - $countOnPage;
        $order = $searchInstance->getSearchOrder();
        $limit = $countOnPage * $page;

        if ($_REQUEST['no_offset'] == "1") {
            $offset = 0;
        }

        $arResult['RESULT'][$object['ID']]['CNT'] = $searchInstance->getResultCount($filter);
        $arResult['RESULT'][$object['ID']]['CNT_WORD'] = plural($arResult['RESULT'][$object['ID']]['CNT'], array('квартиру', 'квартиры', 'квартир'));
        $arResult['RESULT'][$object['ID']]['SHOW_CNT'] = $limit;
        if ($limit > $arResult['RESULT'][$object['ID']]['CNT']) {
            $arResult['RESULT'][$object['ID']]['SHOW_CNT'] = $arResult['RESULT'][$object['ID']]['CNT'];
        }
        $arResult['RESULT'][$object['ID']]['SHOW_CNT_WORD'] = plural($arResult['RESULT'][$object['ID']]['CNT'], array('квартиры', 'квартир', 'квартир'));


        $params = array(
            'order' => $order,
            'limit' => $limit,
            'offset' => $offset,
            'filter' => $filter,
        );
        $flatsInstance = Estate\EstateFlatTable::getInstance();
        $arResult['RESULT'][$object['ID']]['ITEMS'] = $flatsInstance->getFlatsList($params);

//        ob_start();
//        include $_SERVER['DOCUMENT_ROOT'] . '/local/components/kelnik/estate.search/templates/.default/inc_flats.php';
//        $json['html'] = ob_get_clean();


        foreach ($arResult['RESULT'] as $bulding) {
            $flats = array();
            foreach ($bulding['ITEMS'] as $flat) {
                $oneFLat = array();
                $oneFLat['plan'] = $flat['IMAGE_THUMB_MICRO']['src'];
                if ($flat['PLANOPLAN']) {
                    $flat['PLANOPLAN_CONTENT'] = \Bitrix\Estate\Common::getPlanoplanInfo($flat['PLANOPLAN']);
                    if ($flat['PLANOPLAN_CONTENT']['tabs']['2d']['content']) {
                        $flat['PLANOPLAN_CONTENT']['images']['2d'] = \Bitrix\Estate\Common::_resizeImageOrPlaceholder($flat['PLANOPLAN_CONTENT']['tabs']['2d']['content'], 90, 90);
                    }
                }

                if ($flat['PLANOPLAN_CONTENT']['tabs']['2d']['content']) {
                    $oneFLat['plan'] = $flat['PLANOPLAN_CONTENT']['images']['2d'];
                }
                $oneFLat['room'] = $flat['TYPE_NAME'];
                $oneFLat['building'] = $flat['BUILDING']['NAME'];
                $oneFLat['floor'] = $flat['FLOOR']['NAME'];
                $oneFLat['flat'] = $flat['NAME'];
                $oneFLat['totalArea'] = $flat['AREA_TOTAL'];
                $oneFLat['livingSpace'] = $flat['AREA_LIVING'];
                $oneFLat['price'] = $flat['PRICE_TOTAL_F'];
                $oneFLat['link'] = '/kvartiry/flat/' . $flat['ID'] . '/';
                $oneFLat['id'] = $flat['ID'];
                $oneFLat['inFavorite'] = in_array($flat['ID'], $favoriteIds);

                $flats[] = $oneFLat;
            }
            $json['building'][$object['ID']]['flats'] = $flats;
            $json['building'][$object['ID']]['results_cnt'] = $bulding['CNT'];
            $json['building'][$object['ID']]['per_page'] = $countOnPage;
//            $json['building'][$object['ID']]['results_cnt_word'] = $bulding['CNT_WORD'];
//            $json['building'][$object['ID']]['results_show_cnt'] = $bulding['SHOW_CNT'];
//            $json['building'][$object['ID']]['results_show_cnt_word'] = $bulding['SHOW_CNT_WORD'];
            $json['building'][$object['ID']]['results_show_text'] = "Показано " . $bulding['SHOW_CNT'] . " из " . $bulding['CNT'] . " " . $bulding['SHOW_CNT_WORD'];
            $json['building'][$object['ID']]['toggle_btn_text'] = "Показать " . $bulding['CNT'] . " " . $bulding['CNT_WORD'];
            $json['building'][$object['ID']]['page'] = $page;
            $json['building'][$object['ID']]['next_page'] = 0;
            if ($bulding['CNT'] > $bulding['SHOW_CNT']) {
                $json['building'][$object['ID']]['next_page'] = $page + 1;
            }
        }
    }
}

echo json_encode($json);
