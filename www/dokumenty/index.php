<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Проектная документация");
$APPLICATION->SetPageProperty("title", "Документы, разрешение на строительство и проектная декларация ЖК «Московский квартал» ");
$APPLICATION->SetPageProperty("keywords", "документы, разрешение на строительство, проектная декларация жк московский квартал");
$APPLICATION->SetPageProperty("description", "На сайте застройщика жилого комплекса «Московский квартал» смотрите документацию по проекту (разрешение на строительство, проектная декларация).");
?>

<?$APPLICATION->IncludeComponent(
    "kelnik:documents",
    "",
    Array(
        "IBLOCK_ID" => 13,
        "OBJECT_ID" => $_REQUEST['object_id'],
    )
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
