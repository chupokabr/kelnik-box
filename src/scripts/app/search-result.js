define('app/search-result', [
    'jquery',
    'app/tpl/search/table-apartment'
], function($, tableTpl) {
    'use strict';

    var SearchResult = function($content) {
        // Формы всех ЖК
        this.$forms               = $content.find('form');
        // Показать квартиры комплекса (первые 10)
        this.$submit              = $content.find('.j-btn-more');
        // Блок с результатами
        this.$results             = $content.find('.b-building__results');
        // Загрузить ещё квартир комплекса
        this.$moreApartments      = $content.find('.j-building-more');
        // Текст сколько квартир показано
        this.$shownApartmentsText = $content.find('.b-building__shown-text');
        // Таблица с результатами
        this.$table               = $content.find('.b-building__table');
        // Ajax loader
        this.$ajaxLoader          = $('.b-ajax-loader');
        // Ajax loader для добавки внутрь кнопки
        this.$ajaxLoaderInBtn     = this.$ajaxLoader
            .clone()
            .addClass('b-ajax-loader_theme_inside is-active');

        // Кнопки сортировки
        this.$sorts      = $content.find('.j-sort');
        // Форма поиска
        this.$searchForm = $('.j-search-filter').find('form');
        // Поле сортировки
        this.$sortInput  = this.$searchForm.find('input[name=sort]');
        // Направление сортировки
        this.$orderInput = this.$searchForm.find('input[name=order]');
        // Кнопка "Показать результаты"
        this.$resultBtn  = this.$searchForm
            .find('.b-search-filter__submit-btn');
        // Селект сортировки
        this.$selectSort = $content.find('.j-select-sort');

        this.bindEvents();
    };

    SearchResult.prototype.bindEvents = function() {
        this.$submit.on('click', this.openResult.bind(this));
        this.$resultBtn.on('click', this.showResults.bind(this));
        this.$moreApartments.on(
            'click',
            this.bindClickMoreApartments.bind(this)
        );
        this.$sorts.on('click', this.sort.bind(this));

        this.$selectSort.on('change', this.sort.bind(this));
    };

    SearchResult.prototype.showResults = function(e) {
        e.preventDefault();
        var that  = this;
        var $data = this.$searchForm.serialize();
        var objectId;

        $.ajax({
            url       : this.$searchForm.attr('action'),
            type      : this.$searchForm.attr('method') || 'post',
            dataType  : 'json',
            data      : $data,
            beforeSend: function() {
                that.$ajaxLoader.show();
            },
            success: function(data) {
                that.$forms.each(function(indx, element) {
                    objectId = $(element).find('input[name=object]').val();
                    objectId = parseInt(objectId);

                    var $el             = $(element);
                    var $results        = data.building[objectId];
                    var $target         = $el.find('.b-building__table');
                    var page            = $el.find('input[name=page]');
                    var BtnText         = $results['results_show_text'];
                    var $shownApText    = $el.find('.b-building__shown-text');
                    var $moreApartments = $el.find('.j-building-more');
                    var $submit         = $el.find('.j-btn-more');
                    // Устанавливаем текст для кнопки
                    // $submit.data('text', $results['toggle_btn_text']);

                    if ($submit.hasClass('is-active')) {
                        $submit.empty().text('Скрыть');
                    } else {
                        $submit.empty()
                            .text($results['toggle_btn_text']);
                    }
                    // Обновляем таблицу с результатами
                    that.updateApartments($target, $results);
                    if ($results['next_page'] === 0) {
                        $moreApartments.hide();
                    } else {
                        page.val($results['page']);
                        page.attr('data-next', $results['next_page']);
                    }
                    $shownApText.text(BtnText);
                });

                that.$ajaxLoader.hide();
                // Добаляем GET в url
                window.history.pushState(null, null, '?' + $data);
            }
        });
        // Добаляем GET в url
        window.history.pushState(null, null, '?' + $data);
    };

    SearchResult.prototype.openResult = function(e) {
        var $this        = $(e.target);
        var $currentForm = $this.closest('form');
        var $data        = this.$searchForm.serialize() + '&' +
            $currentForm.serialize();
        e.preventDefault();
        $this.next().slideToggle();

        if ($this.hasClass('is-active')) {
            $this.removeClass('is-active')
                .empty()
                .text($this.data('text'));
        } else {
            $this.addClass('is-active').empty().text('Скрыть');
        }

        // Добаляем GET в url
        window.history.pushState(null, null, '?' + $data);
    };

    SearchResult.prototype.bindClickMoreApartments = function(e) {
        e.preventDefault();
        var that                 = this;
        var $target              = $(e.target);
        var $currentForm         = $target.closest('form');
        var objectId             = parseInt($currentForm
            .find('input[name=object]').val());
        var page                 = $currentForm.find('input[name=page]');
        var nextPage             = page.attr('data-next');
        var $shownApartmentsText = $currentForm.find('.b-building__shown-text');
        // Текст на кнопке - "показать ещё"
        let textBtn              = $target.text();

        // Устанавливаем значение следующей страницы
        if (nextPage) {
            page.val(nextPage);
        }

        var $data = this.$searchForm.serialize() + '&' +
            $currentForm.serialize();

        $.ajax({
            url       : this.$searchForm.attr('action'),
            type      : this.$searchForm.attr('method') || 'post',
            dataType  : 'json',
            data      : $data,
            beforeSend: function() {
                that.$moreApartments
                    .empty()
                    .prop('disabled', true)
                    .append(that.$ajaxLoaderInBtn)
                    .addClass('is-disabled');
            },
            success: function(data) {
                that.$moreApartments
                    .empty()
                    .prop('disabled', false)
                    .removeClass('is-disabled')
                    .text(textBtn);

                var $results = data.building[objectId];
                that.downloadApartment($target, $results);
                if ($results['next_page'] === 0) {
                    that.$moreApartments.hide();
                } else {
                    page.val($results['page']);
                    page.attr('data-next', $results['next_page']);
                }
                $shownApartmentsText.text($results['results_show_text']);

                // Добаляем GET в url
                window.history.pushState(null, null, '?' + $data);
            }
        });
    };

    SearchResult.prototype.downloadApartment = function($target, data) {
        var html    = tableTpl(data);
        var parents = $target.parents('.b-building__results');
        parents.find('tbody').append(html);
    };

    SearchResult.prototype.updateApartments = function($target, data) {
        var html    = tableTpl(data);
        var parents = $target.parents('.b-building__results');
        parents.find('tbody').empty().append(html);
    };

    /**
     * Сортировка
     * @param {Event} e
     */
    SearchResult.prototype.sort = function(e) {
        e.preventDefault();
        var that         = this;
        // По какой ссылки кликнули
        var $target      = $(e.target);
        //По какому полю сортировать
        var sort         = $target.data('sort') ||
            $target.find('option:selected').data('sort');
        // Направление сортировки
        var direction    = $target.attr('data-direction') ||
            $target.find('option:selected').data('direction');
        var $currentForm = $target.closest('form');
        var objectId     = parseInt($currentForm
            .find('input[name=object]').val());

        $target.parents('tr').find('.j-sort').each(function() {
            delete $(this)[0].dataset.direction;
            $(this)
                .removeClass('b-building__sort_style_asc')
                .removeClass('b-building__sort_style_desc');
        });

        if (direction === 'asc' || direction === undefined) {
            $target
                .attr('data-direction', 'desc')
                .removeClass('b-building__sort_style_asc')
                .addClass('b-building__sort_style_desc');
            //Присваиваем значение, если direction = undefined
            direction = 'asc';
        } else {
            $target
                .attr('data-direction', 'asc')
                .removeClass('b-building__sort_style_desc')
                .addClass('b-building__sort_style_asc');
        }
        // Устанавливаем направление сортировки
        this.$sortInput.val(sort);
        this.$orderInput.val(direction);

        var $data = this.$searchForm.serialize() + '&' +
            $target.closest('form').serialize();

        $.ajax({
            url       : this.$searchForm.attr('action'),
            type      : this.$searchForm.attr('method') || 'post',
            dataType  : 'json',
            data      : $data + '&no_offset=1',
            beforeSend: function() {
                that.$ajaxLoader.show();
            },
            success: function(data) {
                var $results = data.building[objectId];
                that.updateApartments($target, $results);
                that.$ajaxLoader.hide();
                // Добаляем GET в url
                window.history.pushState(null, null, '?' + $data);
            }
        });
    };

    return SearchResult;
});
